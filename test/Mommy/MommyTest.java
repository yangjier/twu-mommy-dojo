package Mommy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MommyTest {
    @Test
    public void bShouldReturnB(){
        assertEquals("b",new Mommy("b").mummifier());
    }

    @Test
    public void blankShouldReturnBlank(){
        assertEquals(" ",new Mommy(" ").mummifier());
    }

    @Test
    public void aShouldReturnMommy(){
        assertEquals("mommy",new Mommy("a").mummifier());
    }

    @Test
    public void eShouldReturnMommy(){
        assertEquals("mommy",new Mommy("e").mummifier());
    }

    @Test
    public void iShouldReturnMommy(){
        assertEquals("mommy",new Mommy("i").mummifier());
    }

    @Test
    public void oShouldReturnMommy(){
        assertEquals("mommy",new Mommy("o").mummifier());
    }

    @Test
    public void uShouldReturnMommy(){
        assertEquals("mommy",new Mommy("u").mummifier());
    }

    @Test
    public void abShouldReturnMummyb(){
        assertEquals("mommyb",new Mommy("ab").mummifier());
    }

}
