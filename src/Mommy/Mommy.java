package Mommy;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Mommy {
    private String input;

    private final char[] vowels = {'a','e','i','o','u'};

    Mommy(String obj)
    {
        this.input = obj;
    }

    public String mummifier(){
        String result = "";

        for(int i=0;i<input.length();i++){

            char chInput = input.charAt(i);

            boolean isMatch = false;

            for(int j=0;j<vowels.length;j++){

                char chVowels = vowels[j];

                if(chVowels == chInput){

                    isMatch = true;

                    result+="mommy";

                    break;
                }
            }

            if(!isMatch){
                result+= chInput;
            }
        }

        return result;
    }
}
